using GaweDeweStudio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterPage : Page
{
    [SerializeField] private Button charaSatuButton;
    [SerializeField] private Button charaDuaButton;
    [SerializeField] private Button nextButton;

    protected override void Start()
    {
        base.Start();
        nextButton.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Map));
        charaSatuButton.onClick.AddListener(() => GameManager.Instance.characterSelect = 0);
        charaDuaButton.onClick.AddListener(() => GameManager.Instance.characterSelect = 1);
    }
}

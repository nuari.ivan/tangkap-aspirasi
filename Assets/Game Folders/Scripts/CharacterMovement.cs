using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    [SerializeField] private Transform kinectAvatar;

    [SerializeField] private int PlayerNumber;
    [SerializeField] private float moveSpeed;

    [SerializeField] private GameObject[] skins;
    public int characterSkin;

    private Animator[] anims;

    private void Start()
    {
        anims = GetComponentsInChildren<Animator>(true);
        SetupSkin();
    }

    public void SetupSkin()
    {
        foreach (var item in skins)
        {
            item.SetActive(false);
        }
        skins[characterSkin].SetActive(true);
    }

    private void Update()
    {
        if(GameSetting.Instance.isKinectOverride)
        {
            ControlByKinect();
            return;
        }

        if(PlayerNumber == 0)
        {
            if(Input.GetKey(KeyCode.A))
            {
                MoveToLeft();
            }
            else if(Input.GetKey(KeyCode.D))
            {
                MoveToRight();
            }
            else
            {
                foreach (var item in anims)
                {
                    if(item.gameObject.activeInHierarchy)
                    {
                        item.Play("Idle");
                    }
                    
                }
                transform.rotation = Quaternion.Euler(0f, 0f, -0f);
            }
        }

        if (PlayerNumber == 1)
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                MoveToLeft();
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                MoveToRight();
            }
            else
            {
                foreach (var item in anims)
                {
                    if (item.gameObject.activeInHierarchy)
                    {
                        item.Play("Idle");
                    }
                }
                transform.rotation = Quaternion.Euler(0f, 0f, -0f);
            }
        }
    }

    private void ControlByKinect()
    {
        float leftTolerance = transform.position.x - 0.5f;
        float rightTolerance = transform.position.x + 0.5f;

        if (kinectAvatar.position.x < leftTolerance)
        {
            MoveToLeft();
        }
        else if(kinectAvatar.position.x > rightTolerance)
        {
            MoveToRight();
        }
        else
        {
            foreach (var item in anims)
            {
                if (item.gameObject.activeInHierarchy)
                {
                    item.Play("Idle");
                }
            }
            transform.rotation = Quaternion.Euler(0f, 0f, -0f);
        }
    }

    private void MoveToLeft()
    {
        foreach (var item in anims)
        {
            if (item.gameObject.activeInHierarchy)
            {
                item.Play("Walk");
            }
        }
        transform.position += new Vector3(-moveSpeed * Time.deltaTime, 0f, 0f);
        transform.rotation = Quaternion.Euler(0f, 25f, -0f);
    }

    private void MoveToRight()
    {
        foreach (var item in anims)
        {
            if (item.gameObject.activeInHierarchy)
            {
                item.Play("Walk");
            }
        }
        transform.position += new Vector3(moveSpeed * Time.deltaTime, 0f, 0f);
        transform.rotation = Quaternion.Euler(0f, -25f, -0f);
    }
}

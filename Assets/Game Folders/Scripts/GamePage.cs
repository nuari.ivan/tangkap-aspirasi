using GaweDeweStudio;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GamePage : Page
{
    [SerializeField] private GameObject sliderBar;

    [SerializeField] private Image bar;
    [SerializeField] private TMP_Text labelScore;
    [SerializeField] private TMP_Text labelCounter;
    [SerializeField] private TMP_Text labelTimer;

    protected override void Start()
    {
        base.Start();
        GameSetting.Instance.OnScoreAdded += Instance_OnScoreAdded;
        StartCoroutine(StartGame());
    }

    IEnumerator StartGame()
    {
        sliderBar.SetActive(false);
        int n = 3;
        for (int i = 0; i < 4; i++)
        {
            if(n == 3)
            {
                labelCounter.text = $"2 <size=100>+1</size>";
            }if(n == 2)
            {
                labelCounter.text = $"2";
            }if(n == 1)
            {
                labelCounter.text = $"2 <size=100>-1</size>";
            }if(n == 0)
            {
                labelCounter.text = $"Mulai";
            }
            yield return new WaitForSeconds(1f);
            n--;
        }

        GameManager.Instance.ChangeState(GameState.GameStart);
        sliderBar.SetActive(true);
        labelCounter.gameObject.SetActive(false);
        StartCoroutine(MulaiTimer());
    }

    IEnumerator MulaiTimer()
    {
        float timer = 0;
        float maxTimer = 60f;
        switch (GameManager.Instance.currentMode)
        {
            case GameMode.SinglePlayer:
                timer = 45f;
                maxTimer = 45f;
                break;
            case GameMode.DualPlayer:
                timer = 60f;
                maxTimer = 60f;
                break;
        }

        for (int i = 0; i < maxTimer; i++)
        {
            labelTimer.text = $"{timer}";
            float v = timer / maxTimer;
            bar.fillAmount = v;
            yield return new WaitForSeconds(1f);
            timer--;
        }
        GameManager.Instance.ChangeState(GameState.Result);
    }

    private void OnDestroy()
    {
        GameSetting.Instance.OnScoreAdded -= Instance_OnScoreAdded;
    }

    private void Instance_OnScoreAdded(int currentScore)
    {
        labelScore.text = $"{currentScore}";
        
    }
}

using System;
using UnityEngine;

namespace GaweDeweStudio
{
    public class CanvasManager : MonoBehaviour
    {
        [SerializeField] private Page[] allPages;

        private void Start()
        {
            allPages = GetComponentsInChildren<Page>(true);
            GameManager.Instance.OnStateChanged += Instance_OnStateChanged;
        }

        private void OnDestroy()
        {
            GameManager.Instance.OnStateChanged -= Instance_OnStateChanged;
        }

        private void Instance_OnStateChanged(GameState newState)
        {
            switch (newState)
            {
                case GameState.Home:
                    ChangePage(PageName.Home);
                    break;
                case GameState.Mode:
                    ChangePage(PageName.Mode);
                    break;
                case GameState.Character:
                    ChangePage(PageName.Character);
                    break;
                case GameState.Map:
                    ChangePage(PageName.Map);
                    break;
                case GameState.Game:
                    break;
                case GameState.GameStart:
                    break;
                case GameState.Result:
                    ChangePage(PageName.Result);
                    break;
            }
        }

        private void ChangePage(PageName newPage)
        {
            foreach (var item in allPages)
            {
                item.gameObject.SetActive(false);
            }

            Page findPage = Array.Find(allPages, p => p.namaPage == newPage);
            if(findPage != null)
            {
                findPage.gameObject.SetActive(true);
            }
        }
    }
}

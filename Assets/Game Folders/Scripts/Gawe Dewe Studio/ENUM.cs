public enum GameState
{
    Home,
    Mode,
    Character,
    Map,
    Game,
    GameStart,
    Result
}

public enum PageName
{
    Home,
    Mode,
    Character,
    Map,
    Game,
    Result
}

public enum GameMode
{
    SinglePlayer,
    DualPlayer
}

public enum LevelMap
{
    Village,
    FutureCity,
    Harbour
}

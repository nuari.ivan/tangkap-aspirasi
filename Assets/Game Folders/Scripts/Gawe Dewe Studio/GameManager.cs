using System;
using UnityEngine;

namespace GaweDeweStudio
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance;

        public GameState currentState = GameState.Home;
        public GameMode currentMode = GameMode.SinglePlayer;
        public LevelMap currentMap = LevelMap.Village;
        public int characterSelect = 0;

        public delegate void StateChangeDelegate(GameState newState);
        public event StateChangeDelegate OnStateChanged;

        private void Awake()
        {
            if(Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(gameObject);
        }

        public void SetMap(LevelMap newMap)
        {
            currentMap = newMap;
        }

        public void ChangeState(GameState newState)
        {
            if(newState == currentState)
            {
                return;
            }
            currentState = newState;

            OnStateChanged?.Invoke(currentState);
        }

        public void SetGameMode(GameMode newMode)
        {
            currentMode = newMode;
        }
    }
}
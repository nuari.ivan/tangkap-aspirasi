using GaweDeweStudio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapPage : Page
{
    [SerializeField] private Button villageButton;
    [SerializeField] private Button futureCityButton;
    [SerializeField] private Button harbourButton;

    [SerializeField] private string namaSceneNext;

    protected override void Start()
    {
        base.Start();

        villageButton.onClick.AddListener(() =>
        {
            GameManager.Instance.SetMap(LevelMap.Village);
            ChangeScene(namaSceneNext);
        });
        futureCityButton.onClick.AddListener(() =>
        {
            GameManager.Instance.SetMap(LevelMap.FutureCity);
            ChangeScene(namaSceneNext);
        });
        harbourButton.onClick.AddListener(() =>
        {
            GameManager.Instance.SetMap(LevelMap.Harbour);
            ChangeScene(namaSceneNext);
        });
    }
}
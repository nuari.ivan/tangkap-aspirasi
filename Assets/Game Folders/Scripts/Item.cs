using GaweDeweStudio;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Item : MonoBehaviour
{
    [SerializeField] private float moveSpeed;
    [SerializeField , TextArea(2,2)] private string[] allTexts;

    private TMP_Text label;

    private void OnEnable()
    {
        GameManager.Instance.OnStateChanged += Instance_OnStateChanged;
    }

    private void OnDestroy()
    {
        GameManager.Instance.OnStateChanged -= Instance_OnStateChanged;
    }

    private void Instance_OnStateChanged(GameState newState)
    {
        switch (newState)
        {
            case GameState.Result:
                Destroy(gameObject);
                break;
        }
    }

    private void Start()
    {
        label = GetComponent<TMP_Text>();
        label.text = allTexts[Random.Range(0, allTexts.Length)];
    }

    private void Update()
    {
        transform.position += new Vector3(0f, -moveSpeed * Time.deltaTime, 0f);
    }

    private void OnTriggerEnter(Collider other)
    {
        GameSetting.Instance.AddScore();
        Destroy(gameObject);
    }
}
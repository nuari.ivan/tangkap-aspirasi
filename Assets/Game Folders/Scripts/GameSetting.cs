using GaweDeweStudio;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSetting : MonoBehaviour
{
    public static GameSetting Instance;

    public bool isKinectOverride;

    [SerializeField] private int scorePoin;
    [SerializeField] private GameObject[] maps;

    [SerializeField] private CharacterMovement[] allPlayers;
    [SerializeField] private GameObject[] KinectAvatars;

    public delegate void AddScoreDelegate(int currentScore);
    public event AddScoreDelegate OnScoreAdded;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        SetupMap();
        SetupGameMode();
    }

    private void SetupGameMode()
    {
        switch (GameManager.Instance.currentMode)
        {
            case GameMode.SinglePlayer:
                allPlayers[1].gameObject.SetActive(false);
                allPlayers[0].transform.position = new Vector3(0f, -2.5f, 0f);
                allPlayers[0].characterSkin = GameManager.Instance.characterSelect;
                allPlayers[0].SetupSkin();
                SetupAvatar(GameMode.SinglePlayer);
                break;
            case GameMode.DualPlayer:
                allPlayers[0].transform.position = new Vector3(-2.5f, -2.5f, -1.5f);
                allPlayers[1].transform.position = new Vector3(2.5f, -2.5f, 0f);
                allPlayers[1].characterSkin = 1;
                allPlayers[1].SetupSkin();
                SetupAvatar(GameMode.DualPlayer);
                break;
        }
    }

    private void SetupAvatar(GameMode mode)
    {
        isKinectOverride = KinectAvatars.Length > 0;
        if(KinectAvatars.Length <= 0)
        {
            return;
        }

        switch (mode)
        {
            case GameMode.SinglePlayer:
                KinectAvatars[1].SetActive(false);
                break;
            case GameMode.DualPlayer:
                KinectAvatars[0].transform.position = new Vector3(-2.5f, -2.5f, -4.5f);
                KinectAvatars[1].transform.position = new Vector3(2.5f, -2.5f, -3f);
                break;
        }
    }

    private void SetupMap()
    {
        foreach (var item in maps)
        {
            item.SetActive(false);
        }
        switch (GameManager.Instance.currentMap)
        {
            case LevelMap.Village:
                maps[0].SetActive(true);
                break;
            case LevelMap.FutureCity:
                maps[1].SetActive(true);
                break;
            case LevelMap.Harbour:
                maps[2].SetActive(true);
                break;
        }
    }

    public void AddScore()
    {
        AudioManager.Instance.MainkanSuara("Collect");
        scorePoin += 10;
        OnScoreAdded?.Invoke(scorePoin);
    }
}

using GaweDeweStudio;
using UnityEngine;
using UnityEngine.UI;

public class ModePage : Page
{
    [SerializeField] private Button singleButton;
    [SerializeField] private Button dualButton;

    protected override void Start()
    {
        base.Start();

        singleButton.onClick.AddListener(() =>
        {
            GameManager.Instance.SetGameMode(GameMode.SinglePlayer);
            GameManager.Instance.ChangeState(GameState.Character);
        });

        dualButton.onClick.AddListener(() =>
        {
            GameManager.Instance.SetGameMode(GameMode.DualPlayer);
            GameManager.Instance.ChangeState(GameState.Character);
        });
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    [SerializeField] private Sound[] allSounds;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        InitializeSound();
    }

    private void InitializeSound()
    {
        foreach (var item in allSounds)
        {
            item.source = gameObject.AddComponent<AudioSource>();
            item.source.clip = item.klip;
            item.source.volume = item.vol;
            item.source.loop = item.isLooping;
        }
    }

    public void MainkanSuara(string findSound)
    {
        Sound s = Array.Find(allSounds, suara => suara.nama == findSound);
        if(s == null)
        {
            return;
        }
        s.source.Play();
    }
}



[System.Serializable]
public class Sound
{
    public string nama;
    public AudioClip klip;
    public float vol;
    public bool isLooping;

    [HideInInspector]
    public AudioSource source;
}
using GaweDeweStudio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
    [SerializeField] private Item prefabItem;
    [SerializeField] private float xValue;

    private void Start()
    {
        GameManager.Instance.OnStateChanged += Instance_OnStateChanged;
    }

    private void OnDestroy()
    {
        GameManager.Instance.OnStateChanged -= Instance_OnStateChanged;
    }

    private void Instance_OnStateChanged(GameState newState)
    {
        switch (newState)
        {
            case GameState.Home:
                break;
            case GameState.Mode:
                break;
            case GameState.Character:
                break;
            case GameState.Map:
                break;
            case GameState.Game:
                break;
            case GameState.GameStart:
                InvokeRepeating("SpawnItem", 0f, 1.5f);
                break;
            case GameState.Result:
                CancelInvoke();
                break;
        }
    }

    private void SpawnItem()
    {
        float x = Random.Range(-xValue, xValue);
        Vector3 pos = new Vector3(x, 0f, 0f);

        Instantiate(prefabItem, transform.position + pos, Quaternion.identity);
    }
}

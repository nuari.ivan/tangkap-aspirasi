using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimation : MonoBehaviour
{
    [SerializeField] private ParticleSystem fx;

    public void StartFX()
    {
        fx.Play();
    }
}

using GaweDeweStudio;
using System.Collections;
using UnityEngine;

public class ResultPage : Page
{
    [SerializeField] private string nextSceneId;

    protected override void Start()
    {
        base.Start();
        StartCoroutine(ToMenu());
    }

    IEnumerator ToMenu()
    {
        yield return new WaitForSeconds(10f);
        ChangeScene(nextSceneId);
    }
}

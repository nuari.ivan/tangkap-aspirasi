using GaweDeweStudio;
using UnityEngine;
using UnityEngine.UI;

public class MenuPage : Page
{
    [SerializeField] private Button playButton;

    protected override void Start()
    {
        base.Start();
        playButton.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Mode));
    }
}
